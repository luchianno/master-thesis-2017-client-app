﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class ListController : MonoBehaviour
{
    public HashSet<PlateDetection> List;
    public UnityEvent ReloadCompleted;

    public string Path;

    void Awake()
    {
        List = new HashSet<PlateDetection>(new PlateDetection.PlateDetectionComparer());
    }

    void Start()
    {
        TestSerialize();
    }

    // try to load information from server
    public void Reload()
    {
        StartCoroutine(SendRequest());
    }

    IEnumerator SendRequest()
    {
        UnityWebRequest www = UnityWebRequest.Get(Path);
        yield return www.Send();

        if (www.isError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            // deserialize information from server
            var temp = JsonUtility.FromJson<PlateDetection[]>(www.downloadHandler.text);
            foreach (var item in temp)
            {
                // this is hashset, we cant create any dublicates
                List.Add(item);
            }
            ReloadCompleted.Invoke();
        }
    }

    // serialize/deserialize test
    public void TestSerialize()
    {
        var a = new PlateDetection { id = 0, plateNumber = "AA982HH", confidence = 90f, time = DateTime.Now, region = "ge" };
        var b = new PlateDetection { id = 1, plateNumber = "BB000BB", confidence = 96f, time = DateTime.Now, region = "ge" };
        var list = new List<PlateDetection>();
        var json = JsonHelper.ToJson(new PlateDetection[2] { a, b }, true);
        Debug.Log(json);

        var temp = JsonHelper.FromJson<PlateDetection>(json);
        Debug.Log(temp);
        Debug.Log(temp.Length);

        foreach (var item in temp)
        {
            Debug.Log(item.region);
        }
    }

}
