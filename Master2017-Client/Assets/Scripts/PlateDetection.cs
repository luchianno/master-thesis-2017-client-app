﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlateDetection
{
    public int id;
    public DateTime time;
    public string plateNumber;
    public float confidence;

    public string region;

    public float imagePath;

    public class PlateDetectionComparer : IEqualityComparer<PlateDetection>
    {
        public bool Equals(PlateDetection x, PlateDetection y)
        {
            return x.id == y.id;
        }

        public int GetHashCode(PlateDetection obj)
        {
            return obj.id;
        }
    }
}

