﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ListController))]
public class ListControllerEditor : Editor
{
    ListController _target;

    public void OnEnable()
    {
        _target = (ListController)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Reload"))
        {
            _target.Reload();
        }
    }
}
